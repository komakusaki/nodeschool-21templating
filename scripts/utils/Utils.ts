export class Utils {

    public static createElementFromTemplate(template: string): HTMLElement {
        return document.createRange().createContextualFragment(template).firstElementChild as HTMLElement;
    }

    public static findFirstByAttribute(
        container: HTMLElement | DocumentFragment,
        attribute: string,
        value?: string
    ): HTMLElement | null {
        if (container instanceof HTMLElement && container.hasAttribute(attribute)) {
            return container;
        }
        return this.getFirstByAttributeInDOMLevel(
            this.getChildrenOfElements([container] as HTMLElement[]),
            attribute,
            value
        );
    }

    private static getFirstByAttributeInDOMLevel(
        elements: HTMLElement[],
        attribute: string,
        value?: string
    ): HTMLElement | null {
        if (!elements.length) {
            return null;
        }
        for (let element of elements) {
            if (element.hasAttribute(attribute)) {
                if (value) {
                    if (value === element.getAttribute(attribute)) {
                        return element;
                    }
                } else {
                    return element;
                }
            }
        }
        return this.getFirstByAttributeInDOMLevel(this.getChildrenOfElements(elements), attribute, value);
    }

    private static getChildrenOfElements(elements: HTMLElement[]): HTMLElement[] {
        let children: HTMLElement[] = [];
        elements.forEach((element: HTMLElement) => {
            children = children.concat(Array.from(element.children) as HTMLElement[]);
        });
        return children;
    }
}
