import { TextField } from './components/TextField/TextField';
import { LabelField } from './components/LabelField/LabelField';
import { ModelService } from './services/modelService/ModelService';
import { EventBus } from './services/eventBus/EventBus';
import { Elements } from './components/Elements';

export class App {
    constructor() {
        let modelData = {
            personalData: {
                name: 'Krzysztof',
                surname: 'Lipiec'
            },
            attendees: [
                {
                    name: 'Michal'
                },
                {
                    name: 'Bartosz'
                },
                {
                    name: 'Patrycja'
                }
            ]
        };
        let jsonTemplate: IMetadata = {
            "context": "personalData",
            "element": "container-control",
            "children": [
                {
                    "element": "label-control",
                    "context": "name"
                }, {
                    "element": "label-control",
                    "context": "surname"
                },
                {
                    "element": "text-field",
                    "context": "name"
                }
            ]
        };

        let eventBus = new EventBus();
        let modelService = new ModelService(modelData);

        const mainContainer = new Elements['container-control']({
            children: [jsonTemplate]
        }, eventBus);
        mainContainer.setData(modelService.getModel());
        mainContainer.render(document.body);


        // let textField = new TextField(eventBus, 'data.personalData.name');
        // textField.setData(modelService.getModel());
        // textField.render(document.body);
        //
        // let labelField = new LabelField(eventBus, 'data.personalData.name');
        // labelField.setData(modelService.getModel());
        // labelField.render(document.body);
    }
}